# GRAPHQL BFF for crown-clothing project

## Before running

1. Rename `.env.example` file to `.env`
2. Replace firebase variables in `.env`. Get it in your firebase console: Console -> Select project -> 1 app icon -> Settings -> scroll down

```
#firebase database config
API_KEY: '<your_apiKey>'
AUTH_DOMAIN: '<your_authDomain>'
PROJECT_ID: '<your_projectId>'
STORAGE_BUCKET: '<your_storageBucket>'
MESSAGING_SENDER_ID: '<your_messagingSenderId>'
APP_ID: '<your_appId>'
```

3. Generate Google service account key and download the json file
https://console.cloud.google.com/apis/credentials/serviceaccountkey

4. Put service account key json file to the project `service-keys` folder (create it) and replace path in .env file
```
GOOGLE_APPLICATION_CREDENTIALS=<your_accout_key>.json
```

## Run

Run the server with npm run start

Go to playground: http://localhost:5000/graphql