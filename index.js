const { ApolloServer } = require('apollo-server-express')
const express = require('express')
const { readFileSync } = require('fs')
const config = require('./config')
const firebase = require('firebase-admin')

const typeDefs = readFileSync('./schema.graphql', 'UTF-8')
const resolvers = require('./resolvers')

async function start() {
    var app = express()
    let server = null

    const admin = firebase.initializeApp(config.firebaseConfig)
    const db = admin.firestore()

    server = new ApolloServer({
        typeDefs,
        resolvers,
        context: async ({ req }) => {
            const token = req.headers.authorization || ''
            let user = null
            if (token) {
                const accessToken = token.split(' ')[1]
                try {
                    user = await admin.auth().verifyIdToken(accessToken)
                } catch (error) {
                    user = null
                    console.log(error)
                }
                
            }
            return { db, user }
        },
    })
    await server.start()
    server.applyMiddleware({ app })

    app.get('/', (req, res) => res.end('Welcome to the CrownClothingAPI'))
    app.listen({ port: config.port }, () =>
        console.log(
            `GraphQL Server running @ http://localhost:${config.port}${server.graphqlPath}`
        )
    )
}

start()
