const { GraphQLError } = require('graphql')
const { AuthenticationError } = require('apollo-server-express')

module.exports = {
    categories: async (parent, args, { db }) => {
        const snapshot = await db.collection('categories').get()
        const data = []
        snapshot.forEach(doc => data.push({ id: doc.id, ...doc.data() }))
        return data
    },
    category: async (parent, { id }, { db }) => {
        const ref = db.collection('categories').doc(id)
        const category = await ref.get()
        if (!category.exists) {
            throw new GraphQLError('Invalid argument value', {
                extensions: {
                    code: 'BAD_USER_INPUT',
                    argumentName: 'id',
                },
            })
        } else {
            return { id: category.id, ...category.data() }
        }
    },
    getCategoryByTitle: async (parent, { title }, { db }) => {
        const ref = db.collection('categories')
        const snapshot = await ref.where('title', '==', title).get()
        if (snapshot.empty) {
            throw new GraphQLError('No collection found', {
                extensions: {
                    code: 'BAD_USER_INPUT',
                    argumentName: 'title',
                },
            })
        } else {
            const [collection] = snapshot.docs
            return { id: collection.id, ...collection.data() }
        }
    },
    cart: async (parent, args, { db, user }) => {
        if (!user) {
            throw new AuthenticationError('User is unauthorized')
        }
        const ref = db.collection('carts').doc(user.uid)
        const cart = await ref.get()
        return cart.exists
            ? cart.data()
            : { items: [], total: { quantity: 0, price: 0 } }
    },
}
