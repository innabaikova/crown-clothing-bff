const { AuthenticationError } = require('apollo-server-express')
const { GraphQLError } = require('graphql')

const getProductByIdInCategory = async (categoryId, productId, db) => {
    const ref = db.collection('categories').doc(categoryId)
    const category = await ref.get()
    if (!category.exists) {
        throw new GraphQLError('Invalid argument value', {
            extensions: {
                code: 'BAD_USER_INPUT',
                argumentName: 'categoryId',
            },
        })
    }
    return category.data().items.find(item => item.id == productId)
}

const getCartRef = (db, user) => {
    if (!user) {
        throw new AuthenticationError('User is unauthorized')
    }

    return db.collection('carts').doc(user.uid)
}

const updateCart = async (ref, items) => {
    const total = items.reduce(
        (acc, item) => ({
            quantity: acc.quantity + item.quantity,
            price: acc.price + item.quantity * item.price,
        }),
        {
            quantity: 0,
            price: 0,
        }
    )

    await ref.set({ items, total })
    cart = await ref.get()
    return cart.data()
}

module.exports = {
    addToCart: async (parent, { productId, categoryId }, { db, user }) => {
        const ref = getCartRef(db, user)
        let cart = await ref.get()
        let items = []
        let isProductNew = false
        if (cart.exists) {
            items = cart.data().items
            const cartItemIdx = items.findIndex(({ id }) => id == productId)
            isProductNew = cartItemIdx === -1
            if (!isProductNew) items[cartItemIdx].quantity++
        }

        if (isProductNew && !categoryId) {
            throw new GraphQLError('Invalid argument value', {
                extensions: {
                    code: 'BAD_USER_INPUT',
                    argumentName: 'categoryId',
                },
            })
        }

        if (isProductNew) {
            const product = await getProductByIdInCategory(
                categoryId,
                productId,
                db
            )
            items.push({ ...product, quantity: 1 })
        }

        return updateCart(ref, items)
    },
    removeFromCart: async (parent, { productId }, { db, user }) => {
        const ref = getCartRef(db, user)
        let cart = await ref.get()
        let { items } = cart.data()
        const cartItemIdx = items.findIndex(({ id }) => id == productId)
        if (items[cartItemIdx].quantity === 1) {
            items = items.filter(({ id }) => id != productId)
        } else {
            items[cartItemIdx].quantity--
        }

        return updateCart(ref, items)
    },
    clearCartItem: async (parent, { productId }, { db, user }) => {
        const ref = getCartRef(db, user)
        let cart = await ref.get()
        let { items } = cart.data()
        items = items.filter(({ id }) => id != productId)

        return updateCart(ref, items)
    },
    clearCart: async (parent, args, { db, user }) => {
        const ref = getCartRef(db, user)
        ref.delete()
        return { items: [], total: { quantity: 0, price: 0 } }
    }
}
